import React, { useEffect, useState } from 'react';
import { Products, Navbar } from './componets';
import axios from 'axios';

function App() {
  const [products, setProducts] = useState([]);
  const [cart, setCart] = useState([]);

  const fetchProducts = async () => {
    axios.get('http://localhost:8000/api/robots').then(resp => {
      setProducts(resp.data.data);
    });
  };

  const handleAddToCart = async (productId, quantity) => {
    

    setCart();
  };

  useEffect(() => {
    fetchProducts();
  }, []);
  
  return (
    <div className="App">
      <Navbar />
      <Products products={products} onAddToCart={handleAddToCart}/>
    </div>
  );
}

export default App;
